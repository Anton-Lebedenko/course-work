from openpyxl.workbook import Workbook
from openpyxl.writer.excel import save_workbook
import connect_db


def main():

    def enter(name1, place1, place2, vacant):
        for q, row_ in enumerate(name1, place1):
            for w, value_ in enumerate(row_, place2):
                vacant.cell(row=q, column=w).value = value_


    def enter1(name1, place1, place2, vacant, n):
        a_ = 0
        b1_ = 4
        b2_ = 9
        for q, row_ in enumerate(name1, place1):
            for w, value_ in enumerate(row_, place2):
                vacant.cell(row=q + a_, column=w).value = value_
                vacant.merge_cells(n + str(b1_) + ":" + n + str(b2_))
                a_ += 5
                b1_ += 6
                b2_ += 6


    def enter2(name1, place1, place2, vacant):
        a_ = 0
        for q, row_ in enumerate(name1, place1):
            for w, value_ in enumerate(row_, place2):
                vacant.cell(row=q + a_, column=w).value = value_
                a_ += 5


    conn = connect_db.connection
    c = conn.cursor()

    FILE_NAME = 'Вакансії.xlsx'
    file = Workbook()
    for sheet_name in file.sheetnames:
        sheet = file[sheet_name]
        file.remove(sheet)
    vacant1 = file.create_sheet('1-4 курс')
    vacant2 = file.create_sheet('5-6 курс')

    head1 = [
        ['Зведення вакансій'],
        ['Шифр', 'Назва', '', '', 'Всього', 'Бакалаври за курсами навчання'],
        ['', '', '', '', '', 'I', 'ІI', 'ІII', 'IV'],
    ]

    enter(head1, 1, 1, vacant1)

    vacant1.merge_cells('A1:I1')
    vacant1.merge_cells('E2:E3')
    vacant1.merge_cells('F2:I2')
    vacant1.merge_cells('A2:A3')
    vacant1.merge_cells('B2:D3')

    cipher = c.execute("SELECT plan_bach_cipher FROM plan_bachelor")
    enter1(cipher, 4, 1, vacant1, 'A')

    cipher_l = c.execute("SELECT lic_bach_cipher FROM license_bachelor")
    enter1(cipher, 4, 1, vacant1, 'A')

    a = 0
    b1 = 4
    b2 = 9
    count = 0
    for i, row in enumerate(cipher, 4):
        count += 1
        for j, value in enumerate(row, 1):
            vacant1.cell(row=i + a, column=j).value = value
            vacant1.merge_cells('A' + str(b1) + ":" + 'A' + str(b2))
            a += 5
            b1 += 6
            b2 += 6

    names = c.execute("SELECT plan_bach_names FROM plan_bachelor")
    enter1(names, 4, 2, vacant1, 'B')

    group = c.execute("SELECT plan_bach_code FROM plan_bachelor")
    enter1(group, 4, 3, vacant1, 'C')

    about = ['Ліцензія', 'План', 'Б', 'К', 'І', 'Вакансії']
    a = 4
    while count != 0:
        for k, value in enumerate(about, a):
            vacant1.cell(row=k, column=4).value = value

        count -= 1
        a += 6

    year_1_p = c.execute("SELECT plan_bach_first FROM plan_bachelor")
    enter2(year_1_p, 5, 6, vacant1)

    year_2_p = c.execute("SELECT plan_bach_second FROM plan_bachelor")
    enter2(year_2_p, 5, 7, vacant1)

    year_3_p = c.execute("SELECT plan_bach_third FROM plan_bachelor")
    enter2(year_3_p, 5, 8, vacant1)

    year_4_p = c.execute("SELECT plan_bach_fourth FROM plan_bachelor")
    enter2(year_4_p, 5, 9, vacant1)


    year_1_l = c.execute("SELECT lic_bach_first FROM license_bachelor")
    enter2(year_1_l, 4, 6, vacant1)

    year_2_l = c.execute("SELECT lic_bach_second FROM license_bachelor")
    enter2(year_2_l, 4, 7, vacant1)

    year_3_l = c.execute("SELECT lic_bach_third FROM license_bachelor")
    enter2(year_3_l, 4, 8, vacant1)

    year_4_l = c.execute("SELECT lic_bach_fourth FROM license_bachelor")
    enter2(year_4_l, 4, 9, vacant1)

    #####################################################################################

    head2 = [
        ['Зведення вакансій'],
        ['Шифр', 'Назва', 'Всього', 'Магістр'],
        ['', '', '', '2020', '2019'],
    ]
    enter(head2, 1, 1, vacant2)

    vacant2.merge_cells('A1:E1')
    vacant2.merge_cells('D2:E2')
    vacant2.merge_cells('A2:A3')
    vacant2.merge_cells('B2:B3')
    vacant2.merge_cells('C2:C3')

    cipher = c.execute("")
    enter(cipher, 4, 1, vacant2)

    name = c.execute("")
    enter(name, 4, 2, vacant2)

    total = c.execute("")
    enter(total, 4, 3, vacant2)

    year_1 = c.execute("")
    enter(year_1, 4, 4, vacant2)

    year_2 = c.execute("")
    enter(year_2, 4, 5, vacant2)

    save_workbook(file, FILE_NAME)


if __name__ == "__main__":
    main()
