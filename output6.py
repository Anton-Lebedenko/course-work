from openpyxl.workbook import Workbook
from openpyxl.writer.excel import save_workbook
import connect_db
from datetime import datetime

def main():

    # заполнение колонок
    def enter(name, place1, place2, vacant):
        for i_, row_ in enumerate(name, place1):
            for j_, value_ in enumerate(row_, place2):
                vacant.cell(row=i_, column=j_).value = value_


    # заполнение шапки
    def head(vacant):
        head1 = [
            ['Шифр', 'Назва спеціальності', 'місяць', 'кількість випускників', '', '', 'усього'],
            ['', '', '', 'Бюджет', 'Контракт', 'Іноземці', ''],
        ]
        enter(head1, 1, 1, vacant)
        vacant.merge_cells('A1:A2')
        vacant.merge_cells('B1:B2')
        vacant.merge_cells('C1:C2')
        vacant.merge_cells('G1:G2')
        vacant.merge_cells('D1:F1')


    ################################################################################################
    conn = connect_db.connection  # связь с бд
    c = conn.cursor()

    current_datetime = datetime.now()  # текущая дата

    ################################################################################################
    # создание файла и листов
    FILE_NAME = 'Прогноз випуску студентів.xlsx'
    file = Workbook()

    for sheet_name in file.sheetnames:
        sheet = file[sheet_name]
        file.remove(sheet)

    vacant1 = file.create_sheet('Бакалавр')
    vacant2 = file.create_sheet('Магістр')


    ###################################################################################


    def table(vacant, num):
        head(vacant)
        # шифр
        cipher = c.execute("SELECT speciality_number"
                           " FROM speciality;")
        # счетчик
        count = 0

        for i, row in enumerate(cipher, 3):
            count += 1
            for j, value in enumerate(row, 1):
                vacant.cell(row=i, column=j).value = value

        specialty = c.execute("SELECT speciality_name"
                              " FROM speciality;")

        enter(specialty, 3, 2, vacant)

        a = 3
        q = 1
        year = current_datetime.year

        while q != count + 1:
            budget = c.execute("SELECT COUNT(student_id) FROM student_table WHERE degreeID="+num+" AND financingID=1 "
                               "AND  year_lisense = " + str(year) + " "
                                                                    "AND specialityID = " + str(q) + ";")
            enter(budget, a, 4, vacant)

            contract = c.execute("SELECT COUNT(student_id) FROM student_table WHERE degreeID="+num+" AND financingID=2 "
                                 "AND  year_lisense = " + str(year) + " "
                                                                      "AND specialityID = " + str(q) + ";")
            enter(contract, a, 5, vacant)

            total = c.execute("SELECT COUNT(student_id) FROM student_table WHERE degreeID="+num+" "
                              "AND  year_lisense = " + str(year) + " "
                                                                   "AND specialityID = " + str(q) + ";")
            enter(total, a, 7, vacant)

            q += 1
            a += 1


    table(vacant1, "1")
    table(vacant2, "2")

    save_workbook(file, FILE_NAME)


if __name__ == "__main__":
    main()
