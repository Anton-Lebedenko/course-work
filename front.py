import sys
from tkinter import *
from tkinter.ttk import Frame, Label, Style
import tkinter.filedialog as fd
from tkinter import ttk
from PIL import Image, ImageTk
from tkinter import messagebox
from subprocess import call
import pathFiles
import datoTOdb
import output1
import output2
import output3
import output4
import output5
import output6
import output7
import output8


class Example(Frame):

    def center_window(self):
        w = 520
        h = 500

        sw = self.master.winfo_screenwidth()
        sh = self.master.winfo_screenheight()

        x = (sw - w) / 2
        y = (sh - h) / 2
        self.master.geometry('%dx%d+%d+%d' % (w, h, x, y))

    def choose_file(self, num):

        filetypes = (("Файл EXEL", "*.xlsx"),
                     ("Файл EXEL", "*.xls"))
        filename = fd.askopenfilename(title="Открыть файл", initialdir="/",
                                      filetypes=filetypes)
        self.filename[num] = filename
        pathFiles.files[num] = filename
        return filename

    def choose_file_1(self):
        self.choose_file(0)

    def choose_file_2(self):
        self.choose_file(1)

    def choose_file_3(self):
        self.choose_file(2)

    def __init__(self):
        super().__init__()
        self.init_ui()
        self.filename = ['', '', '']

    @staticmethod
    def show_error(msg):
        Tk().withdraw()
        messagebox.showerror("Ошибка", msg)
        sys.exit(1)

    def init_ui(self):
        def checkcmbo():

            # a = call(["python", "datoTOdb.py"] + self.filename)
            a = True
            try:
                datoTOdb.main()
            except Exception:
                a = False


            if combo.get() == "Дані по формі I-7":
                # call(["python", "output1.py"])
                output1.main()

            elif combo.get() == "Перевірка форми 3-2":
                # call(["python", "output2.py"])
                output2.main()

            elif combo.get() == "Контингент студентів/групи":
                # call(["python", "output3.py"])
                output3.main()

            elif combo.get() == "Вакансії":
                # call(["python", "output4.py"])
                output4.main()

            elif combo.get() == "Вакантні місця":
                # call(["python", "output5.py"])
                output5.main()

            elif combo.get() == "Прогноз випуску":
                # call(["python", "output6.py"])
                output6.main()

            elif combo.get() == "Кількість ж/м":
                # call(["python", "output7.py"])
                output7.main()

            elif combo.get() == "Вік студентів":
                # call(["python", "output8.py"])
                output8.main()

            elif combo.get() == "Всі таблиці":
                # call(["python", "output1.py"])
                # call(["python", "output2.py"])
                # call(["python", "output3.py"])
                # call(["python", "output4.py"])
                # call(["python", "output5.py"])
                # call(["python", "output6.py"])
                # call(["python", "output7.py"])
                # call(["python", "output8.py"])
                output1.main()
                output2.main()
                output3.main()
                output4.main()
                output5.main()
                output6.main()
                output7.main()
                output8.main()

            if a == True:
                messagebox.showinfo("Готово!", "Обратобка завершена успішно")

        def button(name, func):
            btn = Button(self,
                         text=name,
                         height=2,
                         width=15,
                         command=func,
                         bg="#0b83b4",
                         fg="#c8dfe4",
                         bd=1
                         )
            return btn

        def section(name, measure):
            chapter = Label(self,
                            text=name,
                            background="white",
                            font=("Arial", measure))
            return chapter

        self.master.title("Інформаційнa системa контингент студентів ДНУ")
        self.center_window()
        style = Style()
        style.configure("TFrame", background="white")
        self.pack(fill=BOTH, expand=True)

        img = Image.open("logo.jpg")
        img = img.resize((200, 200), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(self,
                      image=img,
                      background="white")
        panel.image = img
        panel.grid(row=0, column=1, pady=10, padx=10)

        section_1 = section("Завантаження файлів:", 13)
        section_1.grid(row=1, column=1)

        btn_file_1 = button("Таблиця з ЄДЕБО", self.choose_file_1)
        btn_file_1.grid(row=2, column=0, pady=10, padx=10)

        btn_file_2 = button("Ліцензії", self.choose_file_2)
        btn_file_2.grid(row=2, column=1, pady=10, padx=10)

        btn_file_3 = button("План прийома", self.choose_file_3)
        btn_file_3.grid(row=2, column=2, pady=20, padx=10)

        section_2_1 = section("Таблиці інформаційної системи", 13)
        section_2_2 = section("'контингент студентів ДНУ'", 13)
        section_2_1.grid(row=3, column=1)
        section_2_2.grid(row=4, column=1)

        choice = section("Вибір таблиці:", 11)
        choice.grid(row=5, column=0)

        combo = ttk.Combobox(self,
                             values=[
                                 "Дані по формі I-7",
                                 "Перевірка форми 3-2",
                                 "Контингент студентів/групи",
                                 "Вакансії",
                                 "Вакантні місця",
                                 "Прогноз випуску",
                                 "Кількість ж/м",
                                 "Вік студентів",
                                 "Всі таблиці"
                             ],
                             height=50,
                             width=30,
                             )

        combo.grid(row=5, column=1, pady=20, padx=10)
        combo.current(8)

        btn_file_3 = Button(self,
                            text="Запуск",
                            height=2,
                            width=15,
                            command=checkcmbo,
                            bg="#0b83b4",
                            fg="#c8dfe4",
                            bd=1
                            )
        btn_file_3.grid(row=5, column=2, pady=10, padx=10)

        close = Button(self,
                       text="            Вихід",
                       height=1,
                       width=15,
                       command=self.quit,
                       bg="white",
                       fg="#0b83b4",
                       bd=0,
                       font=("Arial", 11)
                       )
        close.grid(row=6, column=2, pady=30)


if __name__ == "__main__":
    root = Tk()
    e = Example()

    root.mainloop()
