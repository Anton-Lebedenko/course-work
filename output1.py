import openpyxl
from openpyxl.writer.excel import save_workbook
import connect_db


def main():


    def enter(name, place1, place2, vacant):
        for i1, row1 in enumerate(name, place1):
            for j1, value1 in enumerate(row1, place2):
                vacant.cell(row=i1, column=j1).value = value1


    def my_sum(num, place1):
        count_b = c.execute(
            "SELECT COUNT(student_id) FROM student_table WHERE financingID=1 AND formID = " + num + ";")
        enter(count_b, place1, 2, ws)

        count_c = c.execute(
            "SELECT COUNT(student_id) FROM student_table WHERE financingID=2 AND formID = " + num + ";")
        enter(count_c, place1, 3, ws)

    conn = connect_db.connection
    c = conn.cursor()

    FILE_NAME = 'i7.xlsx'

    try:
        wb = openpyxl.load_workbook(FILE_NAME)
    except:
        wb = openpyxl.Workbook()

        for sheet_name in wb.sheetnames:
            sheet = wb[sheet_name]
            wb.remove(sheet)

    ws = wb.create_sheet()

    head = [
        ['', '', 'Форма і7'],
        ['Довідка'],
        ['про контингент ЗВО І_ІV рівнів акредитації'],
        [''],
        [],
        ['Показники', 'Загальний фонд', 'Спеціальний фонд'],
        ['1', '2', '3'],
        ['Кількість студентів - всього'],
        ['в т.ч. денна форма навчання'],
        ['в т.ч. заочна форма навчання'],
        ['в т.ч. вечірня форма навчання'],
        ['Середній приведений контингент (100% - денної, 10% - заочної, 25% - вечірньої)'],
        ['Приведений контингент (%)'],
    ]

    enter(head, 1, 1, ws)

    date_1 = c.execute("SELECT month_d FROM date_of_data ;")
    date = '01.'
    sell = 'cтаном на '
    month = ''
    year = ''

    for i, row in enumerate(date_1, 4):
        for j, value in enumerate(row, 1):
            month += value

    date_2 = c.execute("SELECT year_d FROM date_of_data ;")
    for i, row in enumerate(date_2, 4):
        for j, value in enumerate(row, 1):
            year += value
            date = date + month + '.' + year
            ws.cell(row=i, column=j).value = sell + date + ' p.'

    ws.merge_cells('A2:C2')
    ws.merge_cells('A3:C3')
    ws.merge_cells('A4:C4')

    total_b = c.execute("SELECT COUNT(student_id) FROM student_table WHERE financingID=1;")
    enter(total_b, 8, 2, ws)

    total_c = c.execute("SELECT COUNT(student_id) FROM student_table WHERE financingID=2;")
    enter(total_c, 8, 3, ws)

    my_sum('2', 9)
    my_sum('4', 10)
    my_sum('1', 11)

    ws.cell(12, 2).value = round(
        ws.cell(9, 2).value + ws.cell(10, 2).value * 10 / 100 + ws.cell(11, 2).value * 25 / 100)
    ws.cell(12, 3).value = round(
        ws.cell(9, 3).value + ws.cell(10, 3).value * 10 / 100 + ws.cell(11, 3).value * 25 / 100)
    total_content = ws.cell(12, 2).value + ws.cell(12, 3).value
    ws.cell(13, 2).value = round(ws.cell(12, 2).value * 100 / total_content)
    ws.cell(13, 3).value = round(ws.cell(12, 3).value * 100 / total_content)

    save_workbook(wb, FILE_NAME)


if __name__ == "__main__":
    main()
