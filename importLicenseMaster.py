import pandas
import sys
import pathFiles


def upper_word(data):
    res = []
    for el in data:
        word = el[0]
        word = word.upper()
        el = word + el[1:]
        res.append(el)

    return res

# license_master = pandas.read_excel('C://temp//last/license.xlsx', sheet_name='Магістр', dtype=str)


license_master = pandas.read_excel(pathFiles.files[1], sheet_name='Магістр', dtype=str)


cipher_master = license_master[license_master.columns[0]].tolist()
cipher_master = [" ".join(el.split()) for el in cipher_master]

names_master = license_master[license_master.columns[1]].tolist()
names_master = [el.replace("'", "’") for el in names_master]
names_master = [el.replace("`", "’") for el in names_master]
names_master = [" ".join(el.split()) for el in names_master]
names_master = upper_word(names_master)

code_master = license_master[license_master.columns[2]].tolist()
code_master = [" ".join(el.split()) for el in code_master]

first_master = license_master[license_master.columns[4]].tolist()
first_master = list(map(str, first_master))
first_master = ['0' if el == 'nan' else el for el in first_master]
first_master = [" ".join(el.split()) for el in first_master]

second_master = license_master[license_master.columns[3]].tolist()
second_master = list(map(str, second_master))
second_master = ['0' if el == 'nan' else el for el in second_master]
second_master = [" ".join(el.split()) for el in second_master]

# print(cipher_master)
# print(names_master)
# print(code_master)
# print(first_master)
# print(second_master)
