from openpyxl.workbook import Workbook
import connect_db
from openpyxl.writer.excel import save_workbook


def main():

    def enter(name, place1, place2, ws):
        count = 0
        for i, row1 in enumerate(name, place1):
            for j, value in enumerate(row1, place2):
                ws.cell(row=i, column=j).value = value
                count += 1
        return count


    conn = connect_db.connection
    c = conn.cursor()

    head = [
        ['Контингент', '', '', 'Усього по ДНУ', 'Денна форма навчання', '', '', '', '', '', '', '', '', '', '', '', '', '',
         '', '',
         '', '', 'Усього денна форма навчання', '', '',
         'НМЦЗВФН', '', '', '', '', '', 'Усього', 'Іноземні студенти усіх форм навчання',
         '', '', '', '', '', 'Усього', '', ''],
        ['Шифр', 'Назва спеціальності', '', '',
         '2020', 'І', 'курс', '2019', 'ІІ', 'курс', '2018', 'ІІІ', 'курс', '2017', 'IV', 'курс',
         '2020', 'маг.', '', '2019', 'маг.', '', '', '', '', '2020', '2019', '2018', '2017', '2020M', '2019M', '', '2020',
         '2019', '2018', '2017', '2020M', '2019M', '', '', ''],

    ]

    FILE_NAME = 'Контингент.xlsx'

    file = Workbook()
    for sheet_name in file.sheetnames:
        sheet = file[sheet_name]
        file.remove(sheet)
    ws1 = file.create_sheet('нескорочений термін')
    ws2 = file.create_sheet('скорочений термін')
    ws3 = file.create_sheet('Всього')


    def table(ws, p):
        if p != "":
            p1 = " AND cut_term = " + p + " "
        else:
            p1 = ""
        enter(head, 1, 1, ws)
        ws.merge_cells('R2:S2')
        ws.merge_cells('U2:V2')
        ws.merge_cells('A1:C1')
        ws.merge_cells('B2:C2')
        ws.merge_cells('D1:D2')
        ws.merge_cells('E1:V1')
        ws.merge_cells('W1:Y2')
        ws.merge_cells('Z1:AE1')
        ws.merge_cells('AF1:AF2')
        ws.merge_cells('AG1:AL1')
        ws.merge_cells('AM1:AO2')

        cipher_spec = c.execute("SELECT specialization_number FROM specialization")
        q = enter(cipher_spec, 3, 1, ws)

        cipher_spec = c.execute("SELECT specialization_name FROM specialization")
        enter(cipher_spec, 3, 3, ws)
        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID ="
                                 " " + str(q1) + " " + p1 + ";")
            enter(sum_spec, row, 4, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 1 " + p1 + "")
            enter(sum_spec, row, 5, ws)
            q1 += 1
            row += 1

        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 2  AND course_num = 2 " + p1 + "")
            enter(sum_spec, row, 8, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 2  AND course_num = 3 " + p1 + "")
            enter(sum_spec, row, 11, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 4 " + p1 + "")
            enter(sum_spec, row, 14, ws)
            q1 += 1
            row += 1

        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 5 " + p1 + "")
            enter(sum_spec, row, 17, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 6 " + p1 + "")
            enter(sum_spec, row, 20, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 2 " + p1 + "")
            enter(sum_spec, row, 23, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 1 " + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 1 " + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 26):
                    ws.cell(row=i, column=j).value = int(value) + w
            # enter(sum_spec, row, 26)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 2" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 2" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 27):
                    ws.cell(row=i, column=j).value = int(value) + w
            # enter(sum_spec, row, 26)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 3" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 3" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 28):
                    ws.cell(row=i, column=j).value = int(value) + w
            # enter(sum_spec, row, 26)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 29):
                    ws.cell(row=i, column=j).value = int(value) + w
            # enter(sum_spec, row, 26)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 5" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 5" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 30):
                    ws.cell(row=i, column=j).value = int(value) + w
            # enter(sum_spec, row, 26)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 6" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 6" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 31):
                    ws.cell(row=i, column=j).value = int(value) + w
            # enter(sum_spec, row, 26)
            q1 += 1
            row += 1
        q1 = 1
        row = 3
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specializationID = "
                                 "" + str(q1) + " AND formID = 1" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 32):
                    ws.cell(row=i, column=j).value = int(value) + w
            # enter(sum_spec, row, 26)
            q1 += 1
            row += 1
        ################################################################################################################

        cipher_spec = c.execute("SELECT speciality_number FROM speciality")
        m = q
        q = enter(cipher_spec, 3 + m, 1, ws)
        cipher_spec = c.execute("SELECT speciality_name FROM speciality")
        enter(cipher_spec, 3 + m, 3, ws)
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE "
                                 "specialityID = " + str(q1) + "" + p1 + "")
            enter(sum_spec, row, 4, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 1" + p1 + "")
            enter(sum_spec, row, 5, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 2  AND course_num = 2" + p1 + "")
            enter(sum_spec, row, 8, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID= "
                                 "" + str(q1) + " AND formID = 2  AND course_num = 3" + p1 + "")
            enter(sum_spec, row, 11, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID= "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 4" + p1 + "")
            enter(sum_spec, row, 14, ws)
            q1 += 1
            row += 1

        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 5" + p1 + "")
            enter(sum_spec, row, 17, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 6" + p1 + "")
            enter(sum_spec, row, 20, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 2" + p1 + "")
            enter(sum_spec, row, 23, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 1" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 1" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 26):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 2" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 2" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 27):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 3" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 3" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 28):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 29):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 5" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 5" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 30):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 6" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 6" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 31):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE specialityID = "
                                 "" + str(q1) + " AND formID = 1" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 32):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1

        ####################################################################
        cipher_spec = c.execute("SELECT faculty_name FROM faculty")
        m += q
        q = enter(cipher_spec, 3 + m, 1, ws)

        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = " + str(q1) + "" + p1 + "")
            enter(sum_spec, row, 4, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 1" + p1 + "")
            enter(sum_spec, row, 5, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 2  AND course_num = 2" + p1 + "")
            enter(sum_spec, row, 8, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID= "
                                 "" + str(q1) + " AND formID = 2  AND course_num = 3" + p1 + "")
            enter(sum_spec, row, 11, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID= "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 4" + p1 + "")
            enter(sum_spec, row, 14, ws)
            q1 += 1
            row += 1

        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 5" + p1 + "")
            enter(sum_spec, row, 17, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 2 AND course_num = 6" + p1 + "")
            enter(sum_spec, row, 20, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 2" + p1 + "")
            enter(sum_spec, row, 23, ws)
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 1" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 1" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 26):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 2" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 2" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 27):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 3" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 3" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 28):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 29):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 5" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 5" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 30):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 4 AND course_num = 6" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 1 AND course_num = 6" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 31):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        q1 = 1
        row = 3 + m
        while q1 != q + 1:
            w = 0
            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 4" + p1 + "")
            for i, row1 in enumerate(sum_spec, 6):
                for j, value in enumerate(row1, 6):
                    w += value

            sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE facultyID = "
                                 "" + str(q1) + " AND formID = 1" + p1 + "")
            for i, row1 in enumerate(sum_spec, row):
                for j, value in enumerate(row1, 32):
                    ws.cell(row=i, column=j).value = int(value) + w
            q1 += 1
            row += 1
        #########################################################################################################

        m += q
        ws.cell(row=3 + m, column=1).value = 'Всього по ДНУ'

        row = 3 + m

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table ;")
        enter(sum_spec, row, 4, ws)

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 2 AND course_num = 1" + p1 + "")
        enter(sum_spec, row, 5, ws)

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE  formID = 2  AND course_num = 2" + p1 + "")
        enter(sum_spec, row, 8, ws)

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE  formID = 2  AND course_num = 3" + p1 + "")
        enter(sum_spec, row, 11, ws)

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 2 AND course_num = 4" + p1 + "")
        enter(sum_spec, row, 14, ws)

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE  formID = 2 AND course_num = 5" + p1 + "")
        enter(sum_spec, row, 17, ws)

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 2 AND course_num = 6" + p1 + "")
        enter(sum_spec, row, 20, ws)

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 2" + p1 + "")
        enter(sum_spec, row, 23, ws)

        w = 0
        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 4 AND course_num = 1" + p1 + "")
        for i, row1 in enumerate(sum_spec, 6):
            for j, value in enumerate(row1, 6):
                w += value

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 1 AND course_num = 1" + p1 + "")
        for i, row1 in enumerate(sum_spec, row):
            for j, value in enumerate(row1, 26):
                ws.cell(row=i, column=j).value = int(value) + w

        w = 0
        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE  formID = 4 AND course_num = 2" + p1 + "")
        for i, row1 in enumerate(sum_spec, 6):
            for j, value in enumerate(row1, 6):
                w += value

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE  formID = 1 AND course_num = 2" + p1 + "")
        for i, row1 in enumerate(sum_spec, row):
            for j, value in enumerate(row1, 27):
                ws.cell(row=i, column=j).value = int(value) + w

        w = 0
        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 4 AND course_num = 3" + p1 + "")
        for i, row1 in enumerate(sum_spec, 6):
            for j, value in enumerate(row1, 6):
                w += value

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 1 AND course_num = 3" + p1 + "")
        for i, row1 in enumerate(sum_spec, row):
            for j, value in enumerate(row1, 28):
                ws.cell(row=i, column=j).value = int(value) + w

        w = 0
        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 4 AND course_num = 4" + p1 + "")
        for i, row1 in enumerate(sum_spec, 6):
            for j, value in enumerate(row1, 6):
                w += value

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 1 AND course_num = 4" + p1 + "")
        for i, row1 in enumerate(sum_spec, row):
            for j, value in enumerate(row1, 29):
                ws.cell(row=i, column=j).value = int(value) + w

        w = 0
        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 4 AND course_num = 5" + p1 + "")
        for i, row1 in enumerate(sum_spec, 6):
            for j, value in enumerate(row1, 6):
                w += value

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 1 AND course_num = 5" + p1 + "")
        for i, row1 in enumerate(sum_spec, row):
            for j, value in enumerate(row1, 30):
                ws.cell(row=i, column=j).value = int(value) + w

        w = 0
        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 4 AND course_num = 6" + p1 + "")
        for i, row1 in enumerate(sum_spec, 6):
            for j, value in enumerate(row1, 6):
                w += value

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE  formID = 1 AND course_num = 6" + p1 + "")
        for i, row1 in enumerate(sum_spec, row):
            for j, value in enumerate(row1, 31):
                ws.cell(row=i, column=j).value = int(value) + w

        w = 0
        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 4" + p1 + "")
        for i, row1 in enumerate(sum_spec, 6):
            for j, value in enumerate(row1, 6):
                w += value

        sum_spec = c.execute("SELECT COUNT(student_id) FROM student_table WHERE formID = 1" + p1 + "")
        for i, row1 in enumerate(sum_spec, row):
            for j, value in enumerate(row1, 32):
                ws.cell(row=i, column=j).value = int(value) + w

        ##################################################################################################
        total = m + 1
        a = 3
        while total != 0:
            ws.cell(row=a, column=6).value = '/'
            ws.cell(row=a, column=9).value = '/'
            ws.cell(row=a, column=12).value = '/'
            ws.cell(row=a, column=15).value = '/'
            ws.cell(row=a, column=18).value = '/'
            ws.cell(row=a, column=21).value = '/'
            ws.cell(row=a, column=24).value = '/'
            ws.cell(row=a, column=40).value = '/'
            a += 1
            total -= 1


    table(ws1, "0")
    table(ws2, "1")
    table(ws3, "")
    save_workbook(file, FILE_NAME)


if __name__ == "__main__":
    main()
