from openpyxl.workbook import Workbook
from openpyxl.writer.excel import save_workbook
import connect_db


def main():

    def request(vacant, num):
        budget_b = c.execute(
            "SELECT COUNT(student_id) FROM student_table WHERE formID = " + num + " AND degreeID = 1 AND financingID=1;")
        enter(budget_b, 3, 2, vacant)

        budget_m = c.execute(
            "SELECT COUNT(student_id) FROM student_table WHERE formID = " + num + " AND degreeID = 2 AND financingID=1;")
        enter(budget_m, 3, 3, vacant)

        contract_b = c.execute(
            "SELECT COUNT(student_id) FROM student_table WHERE formID = " + num + " AND degreeID = 1 AND financingID=2;")
        enter(contract_b, 4, 2, vacant)

        contract_m = c.execute(
            "SELECT COUNT(student_id) FROM student_table WHERE formID = " + num + " AND degreeID = 2 AND financingID=2;")
        enter(contract_m, 4, 3, vacant)
        my_sum(vacant)


    def my_sum(vacant):
        vacant.cell(3, 4).value = vacant.cell(3, 2).value + vacant.cell(3, 3).value
        vacant.cell(4, 4).value = vacant.cell(4, 2).value + vacant.cell(4, 3).value
        vacant.cell(5, 2).value = vacant.cell(3, 2).value + vacant.cell(4, 2).value
        vacant.cell(5, 3).value = vacant.cell(4, 3).value + vacant.cell(3, 3).value
        vacant.cell(5, 4).value = vacant.cell(5, 2).value + vacant.cell(5, 3).value


    def margin(ws):
        ws.merge_cells('A1:A2')
        ws.merge_cells('D1:D2')
        ws.merge_cells('B1:C1')


    def head(col):
        heads = [
            [col, 'Кількість студентів', '', 'Усього'],
            ['', 'Бакалавр', 'Магістр', ''],
            ['Бюджет'],
            ['Контракт'],
            ['Усього']
        ]

        return heads


    def enter(name, place1, place2, vacant):
        for i, row in enumerate(name, place1):
            for j, value in enumerate(row, place2):
                vacant.cell(row=i, column=j).value = value


    conn = connect_db.connection
    c = conn.cursor()

    FILE_NAME = 'check_3-2.xlsx'
    file = Workbook()
    for sheet_name in file.sheetnames:
        sheet = file[sheet_name]
        file.remove(sheet)

    vacant1 = file.create_sheet('денна')
    vacant2 = file.create_sheet('заочна')
    vacant3 = file.create_sheet('вечірня')
    vacant4 = file.create_sheet('дистанційна')

    enter(head('Денна форма навчання'), 1, 1, vacant1)
    margin(vacant1)
    request(vacant1, '2')

    ############################################################################################################
    enter(head('Заочна форма навчання'), 1, 1, vacant2)
    margin(vacant2)
    request(vacant2, '4')

    ########################
    enter(head('Вечірня форма навчання'), 1, 1, vacant3)
    margin(vacant3)
    request(vacant3, '1')

    #######################
    enter(head('Дистанційна форма навчання'), 1, 1, vacant4)
    margin(vacant4)
    request(vacant3, '3')

    ########################
    save_workbook(file, FILE_NAME)


if __name__ == "__main__":
    main()
