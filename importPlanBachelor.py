import pandas
import sys
import pathFiles


def upper_word(data):
    res = []
    for el in data:
        word = el[0]
        word = word.upper()
        el = word + el[1:]
        res.append(el)

    return res

# license_bachelor = pandas.read_excel('C://temp/last/plan.xlsx', sheet_name='Бакалавр', dtype=str)
license_bachelor = pandas.read_excel(pathFiles.files[2], sheet_name='Бакалавр', dtype=str)


cipher_bachelor = license_bachelor[license_bachelor.columns[0]].tolist()
cipher_bachelor = [" ".join(el.split()) for el in cipher_bachelor]

names_bachelor = license_bachelor[license_bachelor.columns[1]].tolist()
names_bachelor = [el.replace("'", "’") for el in names_bachelor]
names_bachelor = [el.replace("`", "’") for el in names_bachelor]
names_bachelor = [" ".join(el.split()) for el in names_bachelor]
names_bachelor = upper_word(names_bachelor)

code_bachelor = license_bachelor[license_bachelor.columns[2]].tolist()
code_bachelor = [" ".join(el.split()) for el in code_bachelor]

first_bachelor = license_bachelor[license_bachelor.columns[6]].tolist()
first_bachelor = list(map(str, first_bachelor))
first_bachelor = ['0' if el == 'nan' else el for el in first_bachelor]
first_bachelor = [" ".join(el.split()) for el in first_bachelor]

second_bachelor = license_bachelor[license_bachelor.columns[5]].tolist()
second_bachelor = list(map(str, second_bachelor))
second_bachelor = ['0' if el == 'nan' else el for el in second_bachelor]
second_bachelor = [" ".join(el.split()) for el in second_bachelor]

third_bachelor = license_bachelor[license_bachelor.columns[4]].tolist()
third_bachelor = list(map(str, third_bachelor))
third_bachelor = ['0' if el == 'nan' else el for el in third_bachelor]
third_bachelor = [" ".join(el.split()) for el in third_bachelor]

fourth_bachelor = license_bachelor[license_bachelor.columns[3]].tolist()
fourth_bachelor = list(map(str, fourth_bachelor))
fourth_bachelor = ['0' if el == 'nan' else el for el in fourth_bachelor]
fourth_bachelor = [" ".join(el.split()) for el in fourth_bachelor]

# print(cipher_bachelor)
# print(names_bachelor)
# print(code_bachelor)
# print(first_bachelor)
# print(second_bachelor)
# print(third_bachelor)
# print(fourth_bachelor)
