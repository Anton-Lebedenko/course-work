import connect_db
import importExcel
import importLicenseBachelor
import importLicenseMaster
import importPlanBachelor
import importPlanMaster
from sqlite3 import Error

def to_list_all_first_els(data):
    res = []
    for i in range(0, len(data[0])):
        line = ""
        for j in data:
            line += "'" + str(j[i]) + "', "
        # print(res)
        line = "".join(list(line)[0:-2])
        res.append(line)
    return res

def insert_to_tables(conn, data, query):
    cursor = conn.cursor()
    try:
        res = to_list_all_first_els(data)
        for el in res:
            add_data = query.format(el)
            cursor.execute(add_data)
            conn.commit()
            # print("Query of data executed successfully")

    except Error as error:
        print(error)

    finally:
        if conn:
            cursor.close()
            print("Соединение с MySQL закрыто")

query_lic_bech = "INSERT INTO license_bachelor ('lic_bach_cipher', 'lic_bach_names', 'lic_bach_code', 'lic_bach_first', 'lic_bach_second', 'lic_bach_third', 'lic_bach_fourth') VALUES ({})"

data_to_license_bachelor = []
data_to_license_bachelor.append(importLicenseBachelor.cipher_bachelor)
data_to_license_bachelor.append(importLicenseBachelor.names_bachelor)
data_to_license_bachelor.append(importLicenseBachelor.code_bachelor)
data_to_license_bachelor.append(importLicenseBachelor.first_bachelor)
data_to_license_bachelor.append(importLicenseBachelor.second_bachelor)
data_to_license_bachelor.append(importLicenseBachelor.third_bachelor)
data_to_license_bachelor.append(importLicenseBachelor.fourth_bachelor)

# data_to_license_bachelor = to_list_all_first_els(data_to_license_bachelor)
# print(data_to_license_bachelor)

insert_to_tables(connect_db.connection, data_to_license_bachelor, query_lic_bech)

########################################################################################################################

query_lic_mas = "INSERT INTO license_master ('lic_mas_cipher', 'lic_mas_names', 'lic_mas_code', 'lic_mas_first', 'lic_mas_second') VALUES ({})"

data_to_license_master = []
data_to_license_master.append(importLicenseMaster.cipher_master)
data_to_license_master.append(importLicenseMaster.names_master)
data_to_license_master.append(importLicenseMaster.code_master)
data_to_license_master.append(importLicenseMaster.first_master)
data_to_license_master.append(importLicenseMaster.second_master)

# data_to_license_master = to_list_all_first_els(data_to_license_master)
# print(data_to_license_master)

insert_to_tables(connect_db.connection, data_to_license_master, query_lic_mas)

########################################################################################################################

query_plan_bech = "INSERT INTO plan_bachelor ('plan_bach_cipher', 'plan_bach_names', 'plan_bach_code', 'plan_bach_first', 'plan_bach_second', 'plan_bach_third', 'plan_bach_fourth') VALUES ({})"

data_to_plan_bachelor = []
data_to_plan_bachelor.append(importPlanBachelor.cipher_bachelor)
data_to_plan_bachelor.append(importPlanBachelor.names_bachelor)
data_to_plan_bachelor.append(importPlanBachelor.code_bachelor)
data_to_plan_bachelor.append(importPlanBachelor.first_bachelor)
data_to_plan_bachelor.append(importPlanBachelor.second_bachelor)
data_to_plan_bachelor.append(importPlanBachelor.third_bachelor)
data_to_plan_bachelor.append(importPlanBachelor.fourth_bachelor)

# data_to_plan_bachelor = to_list_all_first_els(data_to_plan_bachelor)
# print(data_to_plan_bachelor)

insert_to_tables(connect_db.connection, data_to_plan_bachelor, query_plan_bech)

########################################################################################################################

query_plan_mas = "INSERT INTO plan_master ('plan_mas_cipher', 'plan_mas_names', 'plan_mas_code', 'plan_mas_first', 'plan_mas_second') VALUES ({})"

data_to_plan_master = []
data_to_plan_master.append(importPlanMaster.cipher_master)
data_to_plan_master.append(importPlanMaster.names_master)
data_to_plan_master.append(importPlanMaster.code_master)
data_to_plan_master.append(importPlanMaster.first_master)
data_to_plan_master.append(importPlanMaster.second_master)

# data_to_plan_master = to_list_all_first_els(data_to_plan_master)
# print(data_to_plan_master)

insert_to_tables(connect_db.connection, data_to_plan_master, query_plan_mas)

query_date_of_data = "INSERT INTO date_of_data ('year_d', 'month_d') VALUES ({})"
date_of_data = []
date_of_data.append(importExcel.year_of_data)
date_of_data.append(importExcel.month_of_data)

insert_to_tables(connect_db.connection, date_of_data, query_date_of_data)
