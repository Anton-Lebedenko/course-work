from openpyxl.workbook import Workbook
from openpyxl.writer.excel import save_workbook
import connect_db


def main():

    def enter(name, place1, place2, vacant):
        for i, row in enumerate(name, place1):
            for j, value in enumerate(row, place2):
                vacant.cell(row=i, column=j).value = value


    conn = connect_db.connection
    c = conn.cursor()
    FILE_NAME = 'Вакантні місця по бюджету.xlsx'

    file = Workbook()
    for sheet_name in file.sheetnames:
        sheet = file[sheet_name]
        file.remove(sheet)
    ##################################################
    vacant1 = file.create_sheet('Для бакалаврів')
    vacant2 = file.create_sheet('для магістрів')
    vacant3 = file.create_sheet('Кількість місць для бакалаврів ')
    vacant4 = file.create_sheet('Кількість місць для магістрів')

    head1 = [
        ['Шифр спеціальності', 'Назва спеціальності', 'Код', 'Курс', '', '', ''],
        ['', '', '', '1', '2', '3', '4'],
    ]
    enter(head1, 1, 1, vacant1)

    vacant1.merge_cells('A1:A2')
    vacant1.merge_cells('B1:B2')
    vacant1.merge_cells('C1:C2')
    vacant1.merge_cells('D1:G1')

    cipher = c.execute("SELECT plan_bach_cipher FROM plan_bachelor;")
    enter(cipher, 3, 1, vacant1)

    specialty = c.execute("SELECT plan_bach_names FROM plan_bachelor;")
    enter(specialty, 3, 2, vacant1)

    shortenTerm = c.execute("")
    enter(shortenTerm, 3, 3, vacant1)

    first = c.execute("")
    enter(first, 3, 4, vacant1)

    second = c.execute("")
    enter(second, 3, 5, vacant1)

    third = c.execute("")
    enter(third, 3, 6, vacant1)

    fourth = c.execute("")
    enter(fourth, 3, 7, vacant1)

    #########лист 2##################

    head2 = [
        ['Шифр спеціальності', 'Назва спеціальності', 'Ознака ОНП', 'Курс', ''],
        ['', '', '', '1', '2'],

    ]

    enter(head2, 1, 1, vacant2)

    vacant2.merge_cells('A1:A2')
    vacant2.merge_cells('B1:B2')
    vacant2.merge_cells('C1:C2')
    vacant2.merge_cells('D1:E1')

    cipher = c.execute("")
    enter(cipher, 3, 1, vacant2)

    specialty = c.execute("")
    enter(specialty, 3, 2, vacant2)

    onp = c.execute("")
    enter(onp, 3, 3, vacant2)

    first = c.execute("")
    enter(first, 3, 4, vacant2)

    second = c.execute("")
    enter(second, 3, 5, vacant2)

    #########лист 3##################
    head3 = [
        ['Шифр спеціальності', 'Назва спеціальності', 'Роки', '', '', ''],
        ['', '', '2021', '2022', '2023', '2024'],
    ]
    enter(head3, 1, 1, vacant3)

    vacant3.merge_cells('A1:A2')
    vacant3.merge_cells('B1:B2')
    vacant3.merge_cells('C1:F1')

    cipher = c.execute("")
    enter(cipher, 3, 1, vacant3)

    specialty = c.execute("")
    enter(specialty, 3, 2, vacant3)

    year_1 = c.execute("")
    enter(year_1, 3, 3, vacant3)

    year_2 = c.execute("")
    enter(year_2, 3, 4, vacant3)

    year_3 = c.execute("")
    enter(year_3, 3, 5, vacant3)

    year_4 = c.execute("")
    enter(year_4, 3, 6, vacant3)

    #########лист 4##################
    head4 = [
        ['Шифр спеціальності', 'Назва спеціальності', 'Роки', ''],
        ['', '', '2021', '2022'],
    ]

    enter(head4, 1, 1, vacant4)

    vacant4.merge_cells('A1:A2')
    vacant4.merge_cells('B1:B2')
    vacant4.merge_cells('C1:D1')

    cipher = c.execute("")
    enter(cipher, 3, 1, vacant4)

    specialty = c.execute("")
    enter(specialty, 3, 2, vacant4)

    year_1 = c.execute("")
    enter(year_1, 3, 3, vacant4)

    year_2 = c.execute("")
    enter(year_2, 3, 4, vacant4)

    save_workbook(file, FILE_NAME)


if __name__ == "__main__":
    main()
