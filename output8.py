from openpyxl.workbook import Workbook
import connect_db

def main():

    def enter(name, place1, place2):
        for k, row in enumerate(name, place1):
            for j, value in enumerate(row, place2):
                ws.cell(row=k, column=j).value = value


    conn = connect_db.connection
    c = conn.cursor()

    head = ['Вік', 'Кількість студентів']

    file = Workbook()
    ws = file.active
    year = 15
    i = 0

    for i, value_h in enumerate(head, 1):
        ws.cell(row=1, column=i).value = value_h

    while year < 34:
        ws.cell(row=i, column=1).value = year
        year += 1
        count_age = c.execute(
            "SELECT COUNT(student_id) FROM student_table WHERE strftime('%Y','now')-strftime('%Y', date_brth)== " +
            str(year) + " ;")
        enter(count_age, i, 2)
        i += 1

    ws.cell(row=21, column=1).value = '34 і старше'

    older = c.execute(
        "SELECT COUNT(student_id) FROM student_table WHERE strftime('%Y','now')-strftime('%Y', date_brth)>=34;")
    enter(older, 21, 2)

    file.save('Кількість студентів по віку.xlsx')

    
if __name__ == "__main__":
    main()
