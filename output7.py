from openpyxl.workbook import Workbook
import connect_db

def main():

    def enter(name, place1, place2):
        for i, row in enumerate(name, place1):
            for j, value in enumerate(row, place2):
                ws.cell(row=i, column=j).value = value


    conn = connect_db.connection
    c = conn.cursor()

    head = [
        ['Назва факультету', 'Кількість студентів', ''],
        ['', 'Жінки', 'Чоловіки'],
    ]

    file = Workbook()
    ws = file.active
    enter(head, 1, 1)

    ws.merge_cells('A1:A2')
    ws.merge_cells('B1:C1')

    faculty = c.execute("SELECT faculty_name FROM faculty;")
    enter(faculty, 3, 1)

    data = c.execute("SELECT COUNT(faculty_id) FROM faculty;").fetchone()

    f = 1
    s = 3

    for i in range(0, data[0]):
        female = c.execute("SELECT COUNT(sexID) FROM student_table WHERE sexID = 1 AND facultyID = '{}';".format(f))
        enter(female, s, 2)

        male = c.execute("SELECT COUNT(sexID) FROM student_table WHERE sexID = 2 AND facultyID = '{}';".format(f))
        enter(male, s, 3)

        f += 1
        s += 1

    file.save('Кількість жінок та чоловіків.xlsx')


if __name__ == "__main__":
    main()
