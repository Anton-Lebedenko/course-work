import pandas
import sys
import datetime
import front
import pathFiles

# excel_data_df = pandas.read_excel('C://temp/last/datalist.xlsx', sheet_name='ЕкспортЗдобувачів_07.04.2021tim')
excel_data_df = pandas.read_excel(pathFiles.files[0], sheet_name='ЕкспортЗдобувачів')

########################################################################################################################
status_name_temp = excel_data_df['Статус навчання'].tolist()
for el in status_name_temp:
    if pandas.isna(el):
        front.Example.show_error("Пустая ячейка: столбик Статус навчання, ячейка {}".format(
            status_name_temp.index(el) + 2))

status_name_temp = [el.lower() for el in status_name_temp]
status_name = list(dict.fromkeys(status_name_temp))
status_name = sorted(status_name, key=str.lower)
# print(status_name)

########################################################################################################################
student_name = excel_data_df['Здобувач'].tolist()
student_name = [el.lower() for el in student_name]
student_name = [el.replace("'", "’") for el in student_name]
student_name = [el.replace("`", "’") for el in student_name]

# print(student_name)

########################################################################################################################
def convert(data, col):
    try:
        for el in data:
            el = str(el)
            el = datetime.datetime.strptime(el, '%Y-%m-%d %H:%M:%S')

    except ValueError:
        front.Example.show_error("Неверная дата - в столбике: {}, ячейке {}".format(col, data.index(el) + 2))


    return data


date_brth_temp = excel_data_df['Дата народження'].tolist()

date_brth = convert(date_brth_temp, 'Дата народження')
date_brth = [str(datetime.datetime.date(x)) for x in date_brth]

# print(date_brth_temp)
# print(date_brth)

########################################################################################################################
sex_temp = excel_data_df['Стать'].tolist()
sex_temp = [el.lower() for el in sex_temp]
sex = list(dict.fromkeys(sex_temp))
sex = sorted(sex, key=str.lower)
# print(sex)

########################################################################################################################
nationality_temp = excel_data_df['Громадянство'].tolist()
nationality_temp = [el.lower() for el in nationality_temp]
nationality = list(dict.fromkeys(nationality_temp))
# print(nationality)

########################################################################################################################
rnokpp = excel_data_df['РНОКПП'].tolist()

# print(rnokpp)

########################################################################################################################
year_lisense = excel_data_df['Рік ліцензійних обсягів'].tolist()

# print(year_lisense)

########################################################################################################################
start_edu_temp = excel_data_df['Початок навчання'].tolist()
start_edu_temp = convert(start_edu_temp, 'Початок навчання')
start_edu = [str(datetime.datetime.date(x)) for x in start_edu_temp]

# print(start_edu)

########################################################################################################################
end_edu_temp = excel_data_df['Завершення навчання'].tolist()
end_edu_temp = convert(end_edu_temp, 'Завершення навчання')
end_edu = [str(datetime.datetime.date(x)) for x in end_edu_temp]

# print(end_edu)

########################################################################################################################
faculty_temp = excel_data_df['Структурний підрозділ'].tolist()
faculty_temp = [el.lower() for el in faculty_temp]
faculty = list(dict.fromkeys(faculty_temp))

# print(faculty)

########################################################################################################################
dual_edu_temp = excel_data_df['Чи здобуває освітній ступінь за дуальною формою навчання'].tolist()
dual_edu_temp = [el.lower() for el in dual_edu_temp]
dual_edu = []
for el in dual_edu_temp:
    if el == 'ні':
        dual_edu.append(0)
    elif el == 'так':
        dual_edu.append(1)

# print(dual_edu)

########################################################################################################################
academic_degree_temp = excel_data_df['Освітній ступінь (рівень)'].tolist()
academic_degree_temp = [el.lower() for el in academic_degree_temp]
academic_degree = list(dict.fromkeys(academic_degree_temp))
academic_degree = sorted(academic_degree, key=str.lower)
# print(academic_degree)

########################################################################################################################
accession_based_temp = excel_data_df['Вступ на основі'].tolist()
accession_based_temp = [el.lower() for el in accession_based_temp]
accession_based = list(dict.fromkeys(accession_based_temp))
accession_based = sorted(accession_based, key=str.lower)

# print(accession_based)

########################################################################################################################
education_form_temp = excel_data_df['Форма навчання'].tolist()
education_form_temp = [el.lower() for el in education_form_temp]
education_form = list(dict.fromkeys(education_form_temp))
education_form = sorted(education_form, key=str.lower)

# print(education_form)

########################################################################################################################
financing_temp = excel_data_df['Джерело фінансування'].tolist()
financing_temp = [el.lower() for el in financing_temp]
financing = list(dict.fromkeys(financing_temp))
financing = sorted(financing, key=str.lower)

# print(financing)

########################################################################################################################
another_spec_temp = excel_data_df['Чи здобувався ступень за іншою спеціальністю'].tolist()
another_spec_temp = [el.lower() for el in another_spec_temp]
another_spec = []
for el in another_spec_temp:
    if el == 'ні':
        another_spec.append(0)
    elif el == 'так':
        another_spec.append(1)

# print(another_spec)

########################################################################################################################
cut_term_temp = excel_data_df['Чи скорочений термін навчання'].tolist()
cut_term_temp = [el.lower() for el in cut_term_temp]
cut_term = []
for el in cut_term_temp:
    if el == 'ні':
        cut_term.append(0)
    elif el == 'так':
        cut_term.append(1)

# print(cut_term)

########################################################################################################################
speciality_for_check = excel_data_df['Спеціальність'].tolist()
speciality_for_check = [el.lower() for el in speciality_for_check]
speciality_split_check = [el.split(' ', 1) for el in speciality_for_check]

speciality_number_check = [el[0] for el in speciality_split_check]
speciality_name_check = [el[1] for el in speciality_split_check]
speciality_name_check = [el.replace("'", "’") for el in speciality_name_check]
speciality_name_check = [el.replace("`", "’") for el in speciality_name_check]

# print(speciality_name_check)

# ----------------------------------------------#

speciality_temp = excel_data_df['Спеціальність'].tolist()
speciality_temp = [el.lower() for el in speciality_temp]
speciality = list(dict.fromkeys(speciality_temp))
# speciality = sorted(speciality)

speciality_split = [el.split(' ', 1) for el in speciality]
speciality_number = [el[0] for el in speciality_split]
speciality_name = [el[1] for el in speciality_split]
speciality_name = [el.replace("'", "’") for el in speciality_name]
speciality_name = [el.replace("`", "’") for el in speciality_name]

# print(speciality)
# print(speciality_split)
# print(speciality_number)
# print(speciality_name)

########################################################################################################################
specialization_for_check = excel_data_df['Спеціалізація'].tolist()
specialization_for_check = list(map(str, specialization_for_check))
specialization_for_check = [x if x != 'nan' else 'none' for x in specialization_for_check]

specialization_split_check = [el.split(' ', 1) if el != 'none' else 'none' for el in specialization_for_check]
# print(specialization_split_check)
specialization_number_check = [el[0] if el != 'none' else 'none' for el in specialization_split_check]
specialization_name_check = [el[1] if el != 'none' else 'none' for el in specialization_split_check]
specialization_name_check = [el.lower() for el in specialization_name_check]
specialization_name_check = [el.replace("'", "’") for el in specialization_name_check]
specialization_name_check = [el.replace("`", "’") for el in specialization_name_check]

# print(specialization_number_check)
# print(specialization_name_check)

# ----------------------------------------------#

specialization_temp = excel_data_df['Спеціалізація'].tolist()
specialization = list(dict.fromkeys(specialization_temp))

specialization = list(map(str, specialization))
specialization = [x if x != 'nan' else 'none' for x in specialization]
specialization = sorted(specialization)

specialization_split = [el.split(' ', 1) if el != 'none' else 'none' for el in specialization]
specialization_number = [el[0] if el != 'none' else 'none' for el in specialization_split]
specialization_name = [el[1] if el != 'none' else 'none' for el in specialization_split]
specialization_name = [el.lower() for el in specialization_name]
specialization_name = [el.replace("'", "’") for el in specialization_name]
specialization_name = [el.replace("`", "’") for el in specialization_name]

# print(specialization)
# print(specialization_split)
# print(specialization_number)
# print(specialization_name)

########################################################################################################################

edu_program_temp = excel_data_df['Освітня програма'].tolist()
edu_program_temp = [el.lower() for el in edu_program_temp]
edu_program_temp = [str(el) for el in edu_program_temp]
edu_program_temp = [el.replace("'", "’") for el in edu_program_temp]
edu_program_temp = [el.replace("`", "’") for el in edu_program_temp]

edu_program = list(dict.fromkeys(edu_program_temp))
edu_program = sorted(edu_program, key=str.lower)
# print(edu_program_temp)
# print(edu_program)
########################################################################################################################

course_num = excel_data_df['Курс'].tolist()

try:
    for el in course_num:
        int(el)
except ValueError:
    front.Example.show_error("Неверное значение - в столбике: Курс, ячейке {}".format(course_num.index(el) + 2))


# print(course_num)

########################################################################################################################
group_for_check = excel_data_df['Група'].tolist()
group_for_check = [el.lower() for el in group_for_check]

try:
    group_split_check = [el.split('-', 2) for el in group_for_check]

    group_name_check = []
    group_year_check = []
    group_number_check = []

    for el in group_split_check:
        group_name_check.append(el[0])
        group_year_check.append(el[1])
        group_number_check.append(el[2])

except IndexError as e:
    front.Example.show_error(
        "Неверное значение - в столбике Группы: {}. {}".format(group_split_check.index(el) + 2, str(e)))


# ----------------------------------------------#

group_temp = excel_data_df['Група'].tolist()
group_temp = [el.lower() for el in group_temp]

group_name = []
group_year = []
group_number = []

try:
    group_ = list(dict.fromkeys(group_temp))
    # print(group_)
    # print(len(group_))
    group_split = [el.split('-', 2) for el in group_]
    # print(group_split)

    for el in group_split:
        group_name.append(el[0])
        group_year.append(el[1])
        group_number.append(el[2])

except IndexError as e:
    front.Example.show_error("Неверное значение - в столбике Группы: {}. {}".format(group_split.index(el) + 2, str(e)))

# print(group_temp)

# print(group_)
# print(group_split)
# print(group_name)
# print(group_year)
# print(group_number)

########################################################################################################################
date_of_data = str(excel_data_df['Дата завантаження'][0])

try:
    date_of_data = datetime.datetime.strptime(date_of_data, '%Y-%m-%d %H:%M:%S')
    date_of_data = str(datetime.datetime.date(date_of_data))[:-3]

except ValueError:
    front.Example.show_error("Неверная дата - в столбике: {}, ячейке {}".format('Дата завантаження', '2'))


date_of_data = date_of_data.split('-', 1)

year_of_data = []
month_of_data = []
year_of_data.append(date_of_data[0])
month_of_data.append(date_of_data[1])
# print(year_of_data)
# print(month_of_data)
